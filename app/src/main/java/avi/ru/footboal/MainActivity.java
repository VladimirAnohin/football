package avi.ru.footboal;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private Integer countTeam1 = 0;
    private Integer countTeam2 = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void add_count_1(View view) {
        countTeam1++;
        TextView counterView = (TextView) findViewById(R.id.countTeam1);
        counterView.setText(countTeam1.toString());
    }

    public void add_count_2(View view) {
        countTeam2++;
        TextView counterView = (TextView) findViewById(R.id.countTeam2);
        counterView.setText(countTeam2.toString());
    }

    public void reset(View view) {
        countTeam1 = 0;
        countTeam2 = 0;
        TextView counterView_1 = (TextView) findViewById(R.id.countTeam2);
        TextView counterView_2 = (TextView) findViewById(R.id.countTeam1);
        counterView_1.setText(countTeam1.toString());
        counterView_2.setText(countTeam2.toString());
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt("countTeam1", countTeam1);
        outState.putInt("countTeam2", countTeam2);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        countTeam1 = savedInstanceState.getInt("countTeam1");
        countTeam2 = savedInstanceState.getInt("countTeam2");
        TextView counterView_1 = (TextView) findViewById(R.id.countTeam1);
        TextView counterView_2 = (TextView) findViewById(R.id.countTeam2);
        counterView_1.setText(countTeam1.toString());
        counterView_2.setText(countTeam2.toString());
    }
}
